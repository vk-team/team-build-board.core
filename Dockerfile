FROM node:13-alpine

RUN apk add --update \
  python \
  python-dev \
  py-pip \
  build-base \
  && pip install virtualenv \
  && rm -rf /var/cache/apk/*

WORKDIR /home/app

COPY package.json .
COPY package-lock.json .
RUN npm i

COPY . .

ENTRYPOINT [ "npm", "start" ]

EXPOSE 3100