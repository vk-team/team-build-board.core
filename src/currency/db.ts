import { getRepository } from 'typeorm';
import { Currency } from '../common/entities/currency';
import { CreateCurrencyDto } from './dto';

export function createCurrency(dto: CreateCurrencyDto) {
  const repository = getRepository(Currency);
  return repository.save(repository.create(dto));
}
export function getCurrencies() {
  return getRepository(Currency).findAndCount();
}
