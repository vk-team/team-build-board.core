import { Currency } from '../common/entities/currency';

export class CreateCurrencyDto implements Omit<Currency, 'id'> {
  name: string;
}
