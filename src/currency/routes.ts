import Router from 'koa-router';
import { AppContext } from '../types';
import { getCurrencies } from './db';

const router = new Router({ prefix: '/currencies' });
router.get('/', async (ctx: AppContext) => {
  const [currencies, count] = await getCurrencies();
  ctx.body = {
    data: currencies,
    count,
  };
});

export default router.routes();
