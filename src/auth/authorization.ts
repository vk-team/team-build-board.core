import HttpStatus from 'http-status-codes';
import { Next } from 'koa';
import composeMiddleware from 'koa-compose';
import createJwtMiddleware from 'koa-jwt';
import { JWT_SECRET } from '../config';
import { AppContext } from '../types';
import { findUser } from '../users/db';

const authorization = (authRequired: boolean = true) =>
  composeMiddleware([
    createJwtMiddleware({
      secret: JWT_SECRET,
      key: 'tokenPayload',
      tokenKey: 'token',
      passthrough: true,
    }),
    async function checkToken(ctx: AppContext, next: Next) {
      if (ctx.state.jwtOriginalError) {
        if (authRequired && ctx.state.jwtOriginalError.message === 'jwt must be provided') {
          ctx.throw(HttpStatus.FORBIDDEN, 'You must provide token');
        }
        if (['invalid signature', 'jwt malformed'].includes(ctx.state.jwtOriginalError.message)) {
          ctx.throw(HttpStatus.UNAUTHORIZED, 'Invalid token');
        }
      }

      if (ctx.state.tokenPayload) {
        const user = await findUser({ id: ctx.state.tokenPayload.userId });
        if (!user) {
          ctx.throw(HttpStatus.UNAUTHORIZED, 'Authenticated user not found');
        }
        ctx.state.authUser = user;
      }

      await next();
    },
  ]);

export default authorization;
