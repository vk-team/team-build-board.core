import * as yup from 'yup';
import { findUser } from '../users/db';

export const registerSchema = yup.object({
  email: yup
    .string()
    .lowercase()
    .trim()
    .test('is-email-existing', 'Email already exist', async email => {
      return email ? !(await findUser({ email })) : true;
    })
    .required('Email is required'),
  fullName: yup
    .string()
    .trim()
    .required('Full name is required'),
  password: yup
    .string()
    .trim()
    .required('Password is required'),
  passwordRepeat: yup
    .string()
    .trim()
    .test('is-equal-to-password', 'Repeat password is not equal to password', function checkRepeatPassword(
      passwordRepeat,
    ) {
      const { password } = this.parent;
      return password ? password && password === passwordRepeat : true;
    })
    .required('Repeat password is required'),
});

export const loginSchema = yup.object({
  email: yup
    .string()
    .lowercase()
    .trim()
    .test('is-email-not-existing', 'Email not found', async email => {
      return email ? Boolean(await findUser({ email })) : true;
    })
    .required('Email is required'),
  password: yup
    .string()
    .trim()
    .required('Password is required'),
});
