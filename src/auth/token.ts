import jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../config';

export function createToken(userId: number) {
  return jwt.sign({ userId }, JWT_SECRET, { expiresIn: '7d' });
}
