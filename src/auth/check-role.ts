import HttpStatus from 'http-status-codes';
import { Next } from 'koa';
import { AppContext } from '../types';
import { ROLES } from '../users/constants';

export function checkRole(roleName: ROLES) {
  return async (ctx: AppContext, next: Next) => {
    if (ctx.state.authUser.role === roleName) {
      await next();
    } else {
      ctx.throw(HttpStatus.FORBIDDEN, 'Forbidden');
    }
  };
}
