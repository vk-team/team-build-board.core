import { CreateUserReqDto } from '../users/dto';

export class SignUpDto extends CreateUserReqDto {
  passwordRepeat: string;
}
export class LoginDto implements Pick<CreateUserReqDto, 'email' | 'password'> {
  email: string;

  password: string;
}
