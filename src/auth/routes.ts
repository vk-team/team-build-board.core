import Router from 'koa-router';
import { ValidationError } from 'yup';
import { AppContext } from '../types';
import { ROLES } from '../users/constants';
import { createUser, findUser } from '../users/db';
import { comparePasswords } from '../users/password';
import { serializeForCreate, serializeToPublic } from '../users/serializer';
import authorization from './authorization';
import { loginSchema, registerSchema } from './schema';
import { createToken } from './token';

const router = new Router({ prefix: '/auth' });
router
  .post('/sign-up', async (ctx: AppContext) => {
    const user = await createUser({
      ...serializeForCreate(await registerSchema.validate(ctx.request.body, { abortEarly: false })),
      role: ROLES.USER,
    });
    ctx.body = {
      data: {
        user: serializeToPublic(user),
        token: createToken(user.id),
      },
    };
  })
  .post('/sign-in', async (ctx: AppContext) => {
    const dto = await loginSchema.validate(ctx.request.body, { abortEarly: false });
    const user = await findUser({ email: dto.email });

    if (!(await comparePasswords(dto.password, user.passwordHash))) {
      throw new ValidationError('Incorrect password', dto.password, 'password');
    }

    ctx.body = {
      data: {
        user: serializeToPublic(user),
        token: createToken(user.id),
      },
    };
  })
  .get('/user', authorization(), (ctx: AppContext) => {
    ctx.body = { data: serializeToPublic(ctx.state.authUser) };
  });

export default router.routes();
