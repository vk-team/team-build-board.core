import Router from 'koa-router';
import authRoutes from './auth/routes';
import categoriesRoutes from './categories/routes';
import commonRoutes from './common/routes';
import currenciesRoutes from './currency/routes';
import cvsRoutes from './cvs/routes';
import locationsRoutes from './locations/routes';
import teamsRoutes from './teams/routes';

const router = new Router();
router.use(commonRoutes);
router.use(authRoutes);
router.use(categoriesRoutes);
router.use(cvsRoutes);
router.use(currenciesRoutes);
router.use(locationsRoutes);
router.use(teamsRoutes);

export default router.routes();
