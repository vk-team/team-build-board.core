import { getRepository } from 'typeorm';
import { Category } from '../common/entities/category';
import { CreateCategoryDto, FindCategoriesDto, FindCategoryDto, UpdateCategoryDto } from './dto';

export async function createCategory(dto: CreateCategoryDto) {
  const repository = getRepository(Category);
  return repository.save(repository.create(dto));
}
export async function findCategory(dto: FindCategoryDto) {
  return getRepository(Category).findOne(dto);
}
export async function updateCategoryById(id: number, dto: UpdateCategoryDto) {
  await getRepository(Category).update({ id }, dto);
  return findCategory({ id });
}
export async function deleteCategoryById(id: number) {
  return getRepository(Category).delete({ id });
}
export async function findCategories(dto?: FindCategoriesDto) {
  const qb = getRepository(Category).createQueryBuilder('category');

  if (dto.ids) {
    qb.andWhere('category.id IN (:...ids)', { ids: dto.ids });
  }
  if (dto.name) {
    qb.andWhere('LOWER(category.name) LIKE :name', { name: `${dto.name.toLowerCase()}%` });
  }
  if (dto.enabled) {
    qb.andWhere('category.enabled = :enabled', { enabled: dto.enabled });
  }
  if (dto.skip) {
    qb.skip(dto.skip);
  }
  if (dto.take) {
    qb.take(dto.take);
  }

  return qb.getManyAndCount();
}
