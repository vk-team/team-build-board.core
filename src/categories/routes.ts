import Router from 'koa-router';
import privateRoutes from './routes.private';
import publicRoutes from './routes.public';

const router = new Router();
router.use(publicRoutes);
router.use(privateRoutes);

export default router.routes();
