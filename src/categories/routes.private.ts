import HttpStatus from 'http-status-codes';
import Router from 'koa-router';
import qs from 'qs';
import authorization from '../auth/authorization';
import { checkRole } from '../auth/check-role';
import { AppContext } from '../types';
import { ROLES } from '../users/constants';
import { createCategory, deleteCategoryById, findCategories, findCategory, updateCategoryById } from './db';
import { categorySchema } from './schema';

const router = new Router({ prefix: '/private/categories' });
router
  .post('/', authorization(), checkRole(ROLES.ADMIN), async (ctx: AppContext) => {
    ctx.body = {
      data: await createCategory(await categorySchema.validate(ctx.request.body, { abortEarly: false })),
    };
  })
  .get('/', authorization(), checkRole(ROLES.ADMIN), async (ctx: AppContext) => {
    const [rows, count] = await findCategories(qs.parse(ctx.request.query));
    ctx.body = {
      data: rows,
      count,
    };
  })
  .put('/:id', authorization(), checkRole(ROLES.ADMIN), async (ctx: AppContext) => {
    const id = parseInt(ctx.params.id, 10);
    const category = await findCategory({ id });
    if (!category) {
      ctx.throw(HttpStatus.BAD_REQUEST, 'Category not found');
    }

    ctx.body = {
      data: await updateCategoryById(
        id,
        await categorySchema.validate({ ...ctx.request.body, id }, { abortEarly: false, context: { id } }),
      ),
    };
  })
  .delete('/:id', authorization(), checkRole(ROLES.ADMIN), async (ctx: AppContext) => {
    const id = parseInt(ctx.params.id, 10);
    const category = await findCategory({ id });
    if (!category) {
      ctx.throw(HttpStatus.BAD_REQUEST, 'Category not found');
    }

    const { affected } = await deleteCategoryById(ctx.params.id);
    ctx.body = {
      data: {
        affected,
      },
    };
  });

export default router.routes();
