import { Category } from '../common/entities/category';
import { Pagination } from '../types';

export class CreateCategoryDto implements Partial<Omit<Category, 'id' | 'children' | 'parent'>> {
  name: string;

  parentId?: number;

  enabled?: boolean;
}
export class FindCategoryDto implements Partial<Omit<Category, 'children' | 'parent'>> {
  id?: number;

  name?: string;

  enabled?: boolean;
}
export class UpdateCategoryDto implements Partial<Omit<Category, 'children' | 'parent'>> {
  name?: string;

  parentId?: number;

  enabled?: boolean;
}
export class FindCategoriesDto implements Partial<Omit<Category, 'id' | 'children' | 'parent'> & Pagination> {
  ids?: number[];

  name?: string;

  enabled?: boolean;

  skip?: number;

  take?: number;
}
