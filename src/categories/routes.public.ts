import HttpStatus from 'http-status-codes';
import Router from 'koa-router';
import qs from 'qs';
import { AppContext } from '../types';
import { findCategories, findCategory } from './db';

const router = new Router({ prefix: '/categories' });
router
  .get('/', async (ctx: AppContext) => {
    const [rows, count] = await findCategories({ ...qs.parse(ctx.request.query), enabled: true });
    ctx.body = {
      data: rows,
      count,
    };
  })
  .get('/:id', async (ctx: AppContext) => {
    const id = parseInt(ctx.params.id, 10);
    const category = await findCategory({ id, enabled: true });
    if (!category) {
      ctx.throw(HttpStatus.BAD_REQUEST, 'Category not found');
    }

    ctx.body = {
      data: category,
    };
  });

export default router.routes();
