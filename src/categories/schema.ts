import * as yup from 'yup';
import { findCategory } from './db';

type Context =
  | undefined
  | {
      id: number;
    };
export const categorySchema = yup.object({
  name: yup
    .string()
    .trim()
    .test('is-category-exist', 'Name already exist', async function checkCategoryName(name) {
      if (name) {
        const category = await findCategory({ name });
        const updatedCategory =
          category && (this.options.context as any) && (this.options.context as any).id === category.id;
        return !category || updatedCategory;
      }

      return true;
    })
    .required('Name is required'),
  parentId: yup.number().test('is-parent-category-exist', "Parent category doesn't exist", async parentId => {
    return parentId ? Boolean(await findCategory({ id: parentId })) : true;
  }),
});
