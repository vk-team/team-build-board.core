import request from 'supertest';
import { LoginDto, SignUpDto } from '../auth/dto';
import { createToken } from '../auth/token';
import { pgConnect, pgDisconnect, pgSync } from '../common/db';
import { createServer } from '../server';
import { ROLES } from '../users/constants';
import { createUser } from '../users/db';

beforeAll(async () => {
  await pgConnect();
  await pgSync();
});
afterAll(async () => {
  await pgSync();
  await pgDisconnect();
});
afterEach(async () => {
  await pgSync();
});

describe('auth e2e', () => {
  describe('POST /auth/sign-up', () => {
    it('return 200 with user and token', async () => {
      const server = createServer();
      const signUpData: SignUpDto = {
        fullName: 'John Wick',
        email: 'john.wick@mail.io',
        password: 'JiojIU',
        passwordRepeat: 'JiojIU',
      };
      const res = await request(server)
        .post('/auth/sign-up')
        .send(signUpData);
      expect(res.status).toBe(200);
      expect(res.body).toEqual({
        data: {
          user: {
            id: expect.any(Number),
            fullName: 'John Wick',
            email: 'john.wick@mail.io',
            role: ROLES.USER,
          },
          token: expect.any(String),
        },
      });
    });
    describe('Validation', () => {
      it('return 400 with no required fields', async () => {
        const server = createServer();
        const res = await request(server)
          .post('/auth/sign-up')
          .send();
        expect(res.status).toBe(400);
        expect(res.body).toEqual({
          message: 'Invalid data',
          validation: {
            email: ['Email is required'],
            fullName: ['Full name is required'],
            password: ['Password is required'],
            passwordRepeat: ['Repeat password is required'],
          },
        });
      });
      it('return 400 with not equal repeat passwords', async () => {
        const server = createServer();
        const signUpData: SignUpDto = {
          fullName: 'John Wick',
          email: 'john.wick@mail.io',
          password: 'JiojIU',
          passwordRepeat: 'JiojIUU',
        };
        const res = await request(server)
          .post('/auth/sign-up')
          .send(signUpData);
        expect(res.status).toBe(400);
        expect(res.body).toEqual({
          message: 'Invalid data',
          validation: {
            passwordRepeat: ['Repeat password is not equal to password'],
          },
        });
      });
      it('return 400 with email already exist', async () => {
        const server = createServer();
        await createUser({
          fullName: 'John Wick',
          email: 'john.wick@mail.io',
          password: 'JiojIU',
          role: ROLES.USER,
        });
        const signUpData: SignUpDto = {
          fullName: 'John Wick',
          email: 'john.wick@mail.io',
          password: 'JiojIU',
          passwordRepeat: 'JiojIU',
        };
        const res = await request(server)
          .post('/auth/sign-up')
          .send(signUpData);
        expect(res.status).toBe(400);
        expect(res.body).toEqual({
          message: 'Invalid data',
          validation: {
            email: ['Email already exist'],
          },
        });
      });
    });
  });

  describe('POST /auth/sing-in', () => {
    describe('Validation', () => {
      it('return 400 with invalid password', async () => {
        const server = createServer();
        await createUser({
          fullName: 'John Wick',
          email: 'john.wick@mail.io',
          password: 'JiojIU',
          role: ROLES.USER,
        });
        const loginData: LoginDto = {
          email: 'john.wick@mail.io',
          password: 'JiojIUU',
        };
        const res = await request(server)
          .post('/auth/sign-in')
          .send(loginData);
        expect(res.status).toBe(400);
        expect(res.body).toEqual({
          message: 'Invalid data',
          validation: {
            password: ['Incorrect password'],
          },
        });
      });
      it('return 400 with email not found', async () => {
        const server = createServer();
        const loginData: LoginDto = {
          email: 'john.wick@mail.io',
          password: 'JiojIU',
        };
        const res = await request(server)
          .post('/auth/sign-in')
          .send(loginData);
        expect(res.status).toBe(400);
        expect(res.body).toEqual({
          message: 'Invalid data',
          validation: {
            email: ['Email not found'],
          },
        });
      });
    });
  });

  describe('/GET /auth/user', () => {
    it('return 200 with authenticated user', async () => {
      await createUser({
        fullName: 'John Wick',
        email: 'john.wick@mail.io',
        password: 'JiojIU',
        role: ROLES.USER,
      });

      const authUser = await createUser({
        fullName: 'Tony Stark',
        email: 'tony.start@email.com',
        password: 'tonystartk',
        role: ROLES.USER,
      });
      const token = createToken(authUser.id);
      const server = createServer();
      const res = await request(server)
        .get('/auth/user')
        .set('Authorization', `Bearer ${token}`);
      expect(res.status).toBe(200);
      expect(res.body).toEqual({
        data: {
          id: expect.any(Number),
          fullName: 'Tony Stark',
          email: 'tony.start@email.com',
          role: 'user',
        },
      });
    });
    describe('Validation', () => {
      it('return 401 with unauthorized error when malformed jwt', async () => {
        const user = await createUser({
          fullName: 'John Wick',
          email: 'john.wick@mail.io',
          password: 'JiojIU',
          role: ROLES.USER,
        });
        const token = createToken(user.id);
        const server = createServer();
        const res = await request(server)
          .get('/auth/user')
          .set('Authorization', `Bearer ${token}-malformed`);
        expect(res.status).toBe(401);
        expect(res.body).toEqual({
          message: 'Invalid token',
        });
      });
      it('return 403 with forbidden error when jwt token not provided', async () => {
        const server = createServer();
        const res = await request(server).get('/auth/user');
        expect(res.status).toBe(403);
        expect(res.body).toEqual({
          message: 'You must provide token',
        });
      });
      it('return 401 with unauthorized error when authenticated user not found', async () => {
        await createUser({
          fullName: 'John Wick',
          email: 'john.wick@mail.io',
          password: 'JiojIU',
          role: ROLES.USER,
        });
        const token = createToken(44);
        const server = createServer();
        const res = await request(server)
          .get('/auth/user')
          .set('Authorization', `Bearer ${token}`);
        expect(res.status).toBe(401);
        expect(res.body).toEqual({
          message: 'Authenticated user not found',
        });
      });
    });
  });
});
