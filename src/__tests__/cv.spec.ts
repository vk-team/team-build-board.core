import request from 'supertest';
import { createToken } from '../auth/token';
import { pgConnect, pgDisconnect, pgSync } from '../common/db';
import { createCurrency } from '../currency/db';
import { createCv } from '../cvs/db';
import { createServer } from '../server';
import { ROLES } from '../users/constants';
import { createUser } from '../users/db';

beforeAll(async () => {
  await pgConnect();
  await pgSync();
});
afterAll(async () => {
  await pgSync();
  await pgDisconnect();
});
afterEach(async () => {
  await pgSync();
});

describe('GET /cv/:id', () => {
  it('return 200 with cv | when cv enabled & user not owner', async () => {
    const currency = await createCurrency({
      name: 'USD',
    });
    const user = await createUser({
      fullName: 'John Wick',
      email: 'john.wick@email.com',
      password: 'johnwick',
      role: ROLES.USER,
    });
    const cv = await createCv({
      title: 'Senior Js Developer',
      description: 'I am top developer',
      currencyId: currency.id,
      salary: 5500,
      userId: user.id,
    });

    const server = createServer();
    const authUser = await createUser({
      fullName: 'Tony Stark',
      email: 'tony.start@email.com',
      password: 'tonystartk',
      role: ROLES.USER,
    });
    const token = createToken(authUser.id);
    const res = await request(server)
      .get(`/cvs/${cv.id}`)
      .set('Authorization', `Bearer ${token}`);

    expect(res.status).toBe(200);
    expect(res.body).toEqual({
      data: expect.objectContaining({
        id: cv.id,
      }),
    });
  });

  it('return 200 with cv | when cv disabled & user owner & request contain enabled query to false', async () => {
    const currency = await createCurrency({
      name: 'USD',
    });
    const authUser = await createUser({
      fullName: 'John Wick',
      email: 'john.wick@email.com',
      password: 'johnwick',
      role: ROLES.USER,
    });
    const cv = await createCv({
      title: 'Senior Js Developer',
      description: 'I am top developer',
      currencyId: currency.id,
      salary: 5500,
      userId: authUser.id,
      enabled: false,
    });

    const server = createServer();
    const token = createToken(authUser.id);
    const res = await request(server)
      .get(`/cvs/${cv.id}?enabled=false`)
      .set('Authorization', `Bearer ${token}`);

    expect(res.status).toBe(200);
  });

  it('return 400 | when cv not enabled & user not owner', async () => {
    const currency = await createCurrency({
      name: 'USD',
    });
    const user = await createUser({
      fullName: 'John Wick',
      email: 'john.wick@email.com',
      password: 'johnwick',
      role: ROLES.USER,
    });
    const cv = await createCv({
      title: 'Senior Js Developer',
      description: 'I am top developer',
      currencyId: currency.id,
      salary: 5500,
      userId: user.id,
      enabled: false,
    });

    const server = createServer();
    const authUser = await createUser({
      fullName: 'Tony Stark',
      email: 'tony.start@email.com',
      password: 'tonystartk',
      role: ROLES.USER,
    });
    const token = createToken(authUser.id);
    const res = await request(server)
      .get(`/cvs/${cv.id}`)
      .set('Authorization', `Bearer ${token}`);

    expect(res.status).toBe(400);
    expect(res.body).toEqual({
      message: 'Cv not found',
    });
  });

  it('return 400 | when cv disabled & user not owner & request contain enabled query to false', async () => {
    const currency = await createCurrency({
      name: 'USD',
    });
    const user = await createUser({
      fullName: 'John Wick',
      email: 'john.wick@email.com',
      password: 'johnwick',
      role: ROLES.USER,
    });
    const cv = await createCv({
      title: 'Senior Js Developer',
      description: 'I am top developer',
      currencyId: currency.id,
      salary: 5500,
      userId: user.id,
      enabled: false,
    });

    const server = createServer();
    const authUser = await createUser({
      fullName: 'Tony Stark',
      email: 'tony.start@email.com',
      password: 'tonystartk',
      role: ROLES.USER,
    });
    const token = createToken(authUser.id);
    const res = await request(server)
      .get(`/cvs/${cv.id}?enabled=false`)
      .set('Authorization', `Bearer ${token}`);

    expect(res.status).toBe(400);
    expect(res.body).toEqual({
      message: 'Cv not found',
    });
  });
});

describe('GET /', () => {
  it('return 200 with enabled cvs | when user not authenticated', async () => {
    const currency = await createCurrency({
      name: 'USD',
    });
    const user = await createUser({
      fullName: 'John Wick',
      email: 'john.wick@email.com',
      password: 'johnwick',
      role: ROLES.USER,
    });
    await Promise.all([
      createCv({
        title: 'Senior Js Developer',
        description: 'I am top developer',
        currencyId: currency.id,
        salary: 5500,
        userId: user.id,
        enabled: false,
      }),
      createCv({
        title: 'Senior Js Developer',
        description: 'I am top developer',
        currencyId: currency.id,
        salary: 5500,
        userId: user.id,
      }),
    ]);

    const server = createServer();
    const res = await request(server).get('/cvs');

    expect(res.body).toEqual({
      data: expect.arrayContaining([expect.objectContaining({ enabled: true })]),
      count: 1,
    });
  });

  it('return 200 with enabled cvs | when user authenticated & pass query with enabled false', async () => {
    const currency = await createCurrency({
      name: 'USD',
    });
    const user = await createUser({
      fullName: 'John Wick',
      email: 'john.wick@email.com',
      password: 'johnwick',
      role: ROLES.USER,
    });
    await Promise.all([
      createCv({
        title: 'Senior Js Developer',
        description: 'I am top developer',
        currencyId: currency.id,
        salary: 5500,
        userId: user.id,
        enabled: false,
      }),
      createCv({
        title: 'Senior Js Developer',
        description: 'I am top developer',
        currencyId: currency.id,
        salary: 5500,
        userId: user.id,
      }),
    ]);

    const server = createServer();
    const token = createToken(user.id);
    const res = await request(server)
      .get('/cvs?enabled=false')
      .set('Authorization', `Bearer ${token}`);

    expect(res.body).toEqual({
      data: expect.arrayContaining([expect.objectContaining({ enabled: true })]),
      count: 1,
    });
  });

  it('return 200 with disabled cvs | when user authenticated & pass query with enabled is false, userId is authUser ', async () => {
    const currency = await createCurrency({
      name: 'USD',
    });
    const user = await createUser({
      fullName: 'John Wick',
      email: 'john.wick@email.com',
      password: 'johnwick',
      role: ROLES.USER,
    });
    await Promise.all([
      createCv({
        title: 'Senior Js Developer',
        description: 'I am top developer',
        currencyId: currency.id,
        salary: 5500,
        userId: user.id,
        enabled: false,
      }),
      createCv({
        title: 'Senior Js Developer',
        description: 'I am top developer',
        currencyId: currency.id,
        salary: 5500,
        userId: user.id,
      }),
    ]);

    const server = createServer();
    const token = createToken(user.id);
    const res = await request(server)
      .get(`/cvs?enabled=false&userId=${user.id}`)
      .set('Authorization', `Bearer ${token}`);

    expect(res.body).toEqual({
      data: expect.arrayContaining([expect.objectContaining({ enabled: false })]),
      count: 1,
    });
  });

  it('return 200 with all cvs | when user authenticated & query userId is authUser ', async () => {
    const currency = await createCurrency({
      name: 'USD',
    });
    const authUser = await createUser({
      fullName: 'John Wick',
      email: 'john.wick@email.com',
      password: 'johnwick',
      role: ROLES.USER,
    });
    await Promise.all([
      createCv({
        title: 'Senior Js Developer',
        description: 'I am top developer',
        currencyId: currency.id,
        salary: 5500,
        userId: authUser.id,
        enabled: false,
      }),
      createCv({
        title: 'Senior Js Developer',
        description: 'I am top developer',
        currencyId: currency.id,
        salary: 5500,
        userId: authUser.id,
      }),
    ]);

    const server = createServer();
    const token = createToken(authUser.id);
    const res = await request(server)
      .get(`/cvs?userId=${authUser.id}`)
      .set('Authorization', `Bearer ${token}`);

    expect(res.body).toEqual({
      data: expect.arrayContaining([
        expect.objectContaining({ enabled: true }),
        expect.objectContaining({ enabled: false }),
      ]),
      count: 2,
    });
  });

  it('return 200 with enabled cvs | when user authenticated & query userId is authUser, enabled is false ', async () => {
    const currency = await createCurrency({
      name: 'USD',
    });
    const user = await createUser({
      fullName: 'John Wick',
      email: 'john.wick@email.com',
      password: 'johnwick',
      role: ROLES.USER,
    });
    await Promise.all([
      createCv({
        title: 'Senior Js Developer',
        description: 'I am top developer',
        currencyId: currency.id,
        salary: 5500,
        userId: user.id,
        enabled: false,
      }),
      createCv({
        title: 'Senior Js Developer',
        description: 'I am top developer',
        currencyId: currency.id,
        salary: 5500,
        userId: user.id,
      }),
    ]);

    const server = createServer();
    const authUser = await createUser({
      fullName: 'Tony Stark',
      email: 'tony.start@email.com',
      password: 'tonystartk',
      role: ROLES.USER,
    });
    const token = createToken(authUser.id);
    const res = await request(server)
      .get(`/cvs?userId=${user.id}`)
      .set('Authorization', `Bearer ${token}`);

    expect(res.body).toEqual({
      data: expect.arrayContaining([expect.objectContaining({ enabled: true })]),
      count: 1,
    });
  });
});
