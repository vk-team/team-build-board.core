import request from 'supertest';
import { createServer } from '../server';

describe('common e2e', () => {
  describe('/health-check', () => {
    describe('GET /health-check', () => {
      it('return 200 with status ok', async () => {
        const server = createServer();
        const res = await request(server).get('/health-check');
        expect(res.status).toBe(200);
        expect(res.body).toEqual({
          data: {
            status: 'ok',
          },
        });
      });
    });
  });
});
