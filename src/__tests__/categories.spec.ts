import request from 'supertest';
import { createToken } from '../auth/token';
import { createCategory } from '../categories/db';
import { CreateCategoryDto } from '../categories/dto';
import { pgConnect, pgDisconnect, pgSync } from '../common/db';
import { createServer } from '../server';
import { ROLES } from '../users/constants';
import { createUser } from '../users/db';

beforeAll(async () => {
  await pgConnect();
  await pgSync();
});
afterAll(async () => {
  await pgSync();
  await pgDisconnect();
});
afterEach(async () => {
  await pgSync();
});

describe('POST /private/categories', () => {
  it('return 400 | when category exist', async () => {
    const categoryData: CreateCategoryDto = {
      name: 'It',
    };
    await createCategory(categoryData);
    const authUser = await createUser({
      fullName: 'John Wick',
      email: 'john.wick@email.com',
      password: 'johnwick',
      role: ROLES.ADMIN,
    });

    const server = createServer();
    const token = createToken(authUser.id);
    const res = await request(server)
      .post('/private/categories')
      .set('Authorization', `Bearer ${token}`)
      .send(categoryData);

    expect(res.status).toBe(400);
    expect(res.body).toEqual({
      message: 'Invalid data',
      validation: {
        name: ['Name already exist'],
      },
    });
  });
});
