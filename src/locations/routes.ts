import Router from 'koa-router';
import { AppContext } from '../types';
import { findCities, findCountries } from './db';

const countriesRouter = new Router({ prefix: '/countries' });
countriesRouter.get('/', async (ctx: AppContext) => {
  const [countries, count] = await findCountries();
  ctx.body = {
    data: countries,
    count,
  };
});

const citiesRouter = new Router({ prefix: '/cities' });
citiesRouter.get('/', async (ctx: AppContext) => {
  const [cities, count] = await findCities();
  ctx.body = {
    data: cities,
    count,
  };
});

const router = new Router();
router.use(countriesRouter.routes());
router.use(citiesRouter.routes());

export default router.routes();
