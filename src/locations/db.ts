import { getRepository } from 'typeorm';
import { City } from '../common/entities/city';
import { Country } from '../common/entities/country';
import { CreateCityDto, CreateCountryDto } from './dto';

export function createCountry(dto: CreateCountryDto) {
  const repository = getRepository(Country);
  return repository.save(repository.create(dto));
}
export function findCountries() {
  return getRepository(Country).findAndCount();
}

export function createCity(dto: CreateCityDto) {
  const repository = getRepository(City);
  return repository.save(repository.create(dto));
}
export function findCities() {
  return getRepository(City).findAndCount();
}
