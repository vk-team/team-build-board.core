import { City } from '../common/entities/city';
import { Country } from '../common/entities/country';

export class CreateCountryDto implements Omit<Country, 'id' | 'cities'> {
  name: string;
}

export class CreateCityDto implements Omit<City, 'id' | 'country'> {
  name: string;

  countryId: number;
}
