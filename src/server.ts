import cors from '@koa/cors';
import http from 'http';
import Koa from 'koa';
import koaBody from 'koa-body';
import { pgConnect } from './common/db';
import error from './common/error';
import { PORT } from './config';
import routes from './routes';
import { AppContext } from './types';

export function createServer() {
  const app = new Koa();
  app.use(cors());
  app.use(error);
  app.use(koaBody());
  app.use(routes);

  app.on('error', (err, ctx: AppContext) => {
    if (ctx.response.status >= 500) {
      console.log(ctx.request.URL);
      console.log(err);
    }
  });

  return http.createServer(app.callback());
}
export async function start() {
  await pgConnect();
  const server = createServer();
  server.listen(PORT, () => {
    console.log(`Server running on ${PORT} port`);
  });
}
