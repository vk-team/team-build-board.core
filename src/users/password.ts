import bcrypt from 'bcrypt';

export async function createPassword(password: string) {
  return bcrypt.hash(password, 10);
}

export async function comparePasswords(password: string, passwordHash: string) {
  return bcrypt.compare(password, passwordHash);
}
