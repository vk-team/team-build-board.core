import { User } from '../common/entities/user';
import { ROLES } from './constants';

export class CreateUserReqDto implements Omit<User, 'id' | 'passwordHash' | 'role'> {
  fullName: string;

  email: string;

  password: string;
}
export class CreateUserDto implements Omit<User, 'id' | 'passwordHash'> {
  fullName: string;

  email: string;

  password: string;

  role: ROLES;
}
export class FindUserDto implements Partial<Pick<User, 'id' | 'email'>> {
  id?: number;

  email?: string;
}
