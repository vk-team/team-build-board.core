import { omit } from 'lodash';
import { SignUpDto } from '../auth/dto';
import { User } from '../common/entities/user';

export function serializeToPublic(user: User) {
  return omit(user, ['passwordHash']);
}

export function serializeForCreate(userBody: SignUpDto) {
  return omit(userBody, ['passwordRepeat']);
}
