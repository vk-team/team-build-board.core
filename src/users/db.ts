import { getRepository } from 'typeorm';
import { User } from '../common/entities/user';
import { CreateUserDto, FindUserDto } from './dto';
import { createPassword } from './password';

export async function createUser({ password, ...userData }: CreateUserDto) {
  const repository = getRepository(User);
  return repository.save(
    repository.create({
      ...userData,
      passwordHash: await createPassword(password),
    }),
  );
}

export async function findUser(dto: FindUserDto) {
  return getRepository(User).findOne(dto);
}
