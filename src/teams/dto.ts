import { Team } from '../common/entities/team';
import { Pagination } from '../types';

export class CreateTeamDto
  implements Omit<Team, 'users' | 'id' | 'city' | 'country' | 'categories' | 'afterLoad' | 'owner'> {
  name: string;

  enabled: boolean;

  countryId: number;

  cityId: number;

  ownerId: number;

  description: string;

  categoriesIds?: number[];

  usersIds?: number[];
}

export class FindTeams
  implements
    Partial<
      Omit<Team, 'id' | 'users' | 'id' | 'city' | 'country' | 'categories' | 'afterLoad' | 'owner'> & Pagination
    > {
  ids?: number[];

  name?: string;

  enabled?: boolean;

  countryId?: number;

  cityId?: number;

  ownerId?: number;

  usersIds?: number[];

  categoriesIds?: number[];

  skip?: number;

  take?: number;
}
