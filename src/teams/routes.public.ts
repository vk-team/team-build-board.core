import HttpStatus from 'http-status-codes';
import Router from 'koa-router';
import qs from 'qs';
import authorization from '../auth/authorization';
import { AppContext } from '../types';
import { createTeam, deleteTeamById, findTeam, findTeams, updateTeamById } from './db';

const router = new Router({ prefix: '/teams' });
router
  .post('/', authorization(), async (ctx: AppContext) => {
    ctx.body = {
      data: await createTeam({ ...ctx.request.body, ownerId: ctx.state.authUser.id }),
    };
  })
  .get('/', authorization(false), async (ctx: AppContext) => {
    const query = { ...qs.parse(ctx.request.query), enabled: true };
    query.ownerId = query.ownerId && parseInt(ctx.query.ownerId, 10);
    if (query.ownerId && ctx.state.authUser && query.ownerId === ctx.state.authUser.id) {
      query.enabled = ctx.query.enabled;
    }

    const [cvs, count] = await findTeams(query);
    ctx.body = {
      data: cvs,
      count,
    };
  })
  .put('/:id', authorization(), async (ctx: AppContext) => {
    const id = parseInt(ctx.params.id, 10);
    const team = await findTeam({ id, ownerId: ctx.state.authUser.id });
    if (!team) {
      ctx.throw(HttpStatus.BAD_REQUEST, 'Team not found');
    }

    ctx.body = {
      data: await updateTeamById(id, { ...ctx.request.body, ownerId: ctx.state.authUser.id }),
    };
  })
  .get('/:id', authorization(false), async (ctx: AppContext) => {
    const id = parseInt(ctx.params.id, 10);
    const team = await findTeam({ id });
    const userOwner = ctx.state.authUser && ctx.state.authUser.id === team.ownerId;
    if (team && (userOwner || team.enabled === true)) {
      ctx.body = {
        data: team,
      };
    } else {
      ctx.throw(HttpStatus.BAD_REQUEST, 'Team not found');
    }
  })
  .delete('/:id', authorization(), async (ctx: AppContext) => {
    const id = parseInt(ctx.params.id, 10);
    const team = await findTeam({ id, ownerId: ctx.state.authUser.id });
    if (!team) {
      ctx.throw(HttpStatus.BAD_REQUEST, 'Team not found');
    }

    const { affected } = await deleteTeamById(id);
    ctx.body = {
      data: { affected, id },
    };
  });

export default router.routes();
