import { getRepository } from 'typeorm';
import { Team } from '../common/entities/team';
import { CreateTeamDto, FindTeams } from './dto';

export async function findTeam(dto: Partial<Pick<Team, 'id' | 'ownerId'>>) {
  const qb = getRepository(Team).createQueryBuilder('team');

  if (dto.id) {
    qb.andWhere('team.id = :id', { id: dto.id });
  }

  qb.leftJoinAndSelect('team.owner', 'owner');
  qb.leftJoinAndSelect('team.users', 'users');
  qb.leftJoinAndSelect('team.categories', 'categories');
  qb.leftJoinAndSelect('team.country', 'country');
  qb.leftJoinAndSelect('team.city', 'city');

  return qb.getOne();
}

export async function createTeam({ categoriesIds, usersIds, ...dto }: CreateTeamDto) {
  const qb = getRepository(Team).createQueryBuilder('team');

  const {
    identifiers: [{ id }],
  } = await qb
    .insert()
    .values(dto)
    .execute();

  if (categoriesIds) {
    await qb
      .relation('categories')
      .of(id)
      .add(categoriesIds);
  }

  if (usersIds) {
    await qb
      .relation('users')
      .of(id)
      .add(usersIds);
  }

  return findTeam({ id });
}

export async function findTeams(dto: FindTeams) {
  const qb = getRepository(Team).createQueryBuilder('team');

  if (dto.name) {
    qb.andWhere('LOWER(team.name) LIKE :name', { title: `${dto.name.toLowerCase()}%` });
  }
  if (dto.enabled) {
    qb.andWhere('team.enabled = :enabled', { enabled: dto.enabled });
  }
  if (dto.countryId) {
    qb.andWhere('team.countryId = :countryId', { countryId: dto.countryId });
  }
  if (dto.cityId) {
    qb.andWhere('team.cityId = :cityId', { cityId: dto.cityId });
  }
  if (dto.ownerId) {
    qb.andWhere('team.ownerId = :ownerId', { ownerId: dto.ownerId });
  }

  qb.leftJoinAndSelect('team.owner', 'owner');

  if (dto.usersIds) {
    qb.leftJoinAndSelect('team.users', 'users', 'users.id IN (:...usersIds)', {
      usersIds: dto.usersIds,
    });
  } else {
    qb.leftJoinAndSelect('team.users', 'users');
  }

  if (dto.categoriesIds) {
    qb.leftJoinAndSelect('team.categories', 'categories', 'categories.id IN (:...categoriesIds)', {
      categoriesIds: dto.categoriesIds,
    });
  } else {
    qb.leftJoinAndSelect('team.categories', 'categories');
  }

  qb.leftJoinAndSelect('team.country', 'country');
  qb.leftJoinAndSelect('team.city', 'city');

  qb.skip(dto.skip);
  qb.take(dto.take);

  return qb.getManyAndCount();
}

export async function updateTeamById(id: number, { categoriesIds, usersIds, ...dto }: Partial<CreateTeamDto>) {
  const qb = getRepository(Team).createQueryBuilder('team');

  await qb
    .update()
    .where('team.id = :id', { id })
    .set(dto)
    .execute();

  if (categoriesIds) {
    const categories = await qb
      .relation('categories')
      .of(id)
      .loadMany();
    await qb
      .relation('categories')
      .of(id)
      .remove(categories);
    await qb
      .relation('categories')
      .of(id)
      .add(categoriesIds);
  }

  if (usersIds) {
    const users = await qb
      .relation('users')
      .of(id)
      .loadMany();
    await qb
      .relation('users')
      .of(id)
      .remove(users);
    await qb
      .relation('users')
      .of(id)
      .add(usersIds);
  }

  return findTeam({ id });
}

export async function deleteTeamById(id: number) {
  return getRepository(Team)
    .createQueryBuilder('team')
    .delete()
    .where('team.id = :id', { id })
    .execute();
}
