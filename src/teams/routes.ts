import Router from 'koa-router';
import publicRoutes from './routes.public';

const router = new Router();
router.use(publicRoutes);

export default router.routes();

