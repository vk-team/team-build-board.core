import dotenv from 'dotenv';

dotenv.config();

export const PORT = parseInt(process.env.PORT, 10);
export const PG_URL = String(process.env.PG_URL);
export const JWT_SECRET = String(process.env.JWT_SECRET);
