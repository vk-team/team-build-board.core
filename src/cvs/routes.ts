import Router from 'koa-router';
import privateRouter from './routes.private';
import publicRoutes from './routes.public';

const router = new Router();
router.use(privateRouter);
router.use(publicRoutes);

export default router.routes();
