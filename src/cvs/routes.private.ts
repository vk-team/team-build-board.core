import HttpStatus from 'http-status-codes';
import Router from 'koa-router';
import qs from 'qs';
import authorization from '../auth/authorization';
import { checkRole } from '../auth/check-role';
import { AppContext } from '../types';
import { ROLES } from '../users/constants';
import { createCv, findCv, findCvs, updateCvById } from './db';

const router = new Router({ prefix: '/private/cvs' });
router
  .post('/', authorization(), checkRole(ROLES.ADMIN), async (ctx: AppContext) => {
    ctx.body = {
      data: await createCv(ctx.request.body),
    };
  })
  .get('/', authorization(), checkRole(ROLES.ADMIN), async (ctx: AppContext) => {
    const [cvs, count] = await findCvs(qs.parse(ctx.query));
    ctx.body = {
      data: cvs,
      count,
    };
  })
  .put('/:id', authorization(), checkRole(ROLES.ADMIN), async (ctx: AppContext) => {
    const id = parseInt(ctx.params.id, 10);
    const cv = await findCv({ id });
    if (!cv) {
      ctx.throw(HttpStatus.BAD_REQUEST, 'Cv not found');
    }

    ctx.body = {
      data: await updateCvById(id, ctx.request.body),
    };
  });

export default router.routes();
