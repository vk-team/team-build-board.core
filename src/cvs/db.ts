import { getRepository } from 'typeorm';
import { Cv } from '../common/entities/cv';
import { CreateCvDto, FindCv, FindCvs } from './dto';

export async function findCv(dto: FindCv) {
  const qb = getRepository(Cv).createQueryBuilder('cv');

  if (dto.id) {
    qb.andWhere('cv.id = :id', { id: dto.id });
  }

  qb.leftJoinAndSelect('cv.user', 'user');
  qb.leftJoinAndSelect('cv.categories', 'categories');
  qb.leftJoinAndSelect('cv.country', 'country');
  qb.leftJoinAndSelect('cv.city', 'city');
  qb.leftJoinAndSelect('cv.currency', 'currency');

  return qb.getOne();
}

export async function createCv({ categoriesIds, ...dto }: CreateCvDto) {
  const qb = getRepository(Cv).createQueryBuilder('cv');

  const {
    identifiers: [{ id }],
  } = await qb
    .insert()
    .values(dto)
    .execute();

  if (categoriesIds) {
    await qb
      .relation('categories')
      .of(id)
      .add(categoriesIds);
  }

  return findCv({ id });
}

export async function updateCvById(id: number, { categoriesIds, ...dto }: Partial<CreateCvDto>) {
  const qb = getRepository(Cv).createQueryBuilder('cv');
  await qb
    .update()
    .where('cv.id = :id', { id })
    .set(dto)
    .execute();

  if (categoriesIds) {
    const categories = await qb
      .relation('categories')
      .of(id)
      .loadMany();
    await qb
      .relation('categories')
      .of(id)
      .remove(categories);
    await qb
      .relation('categories')
      .of(id)
      .add(categoriesIds);
  }

  return findCv({ id });
}

export async function deleteCvById(id: number) {
  return getRepository(Cv)
    .createQueryBuilder('cv')
    .delete()
    .where('cv.id = :id', { id })
    .execute();
}

export async function findCvs(dto: FindCvs) {
  const qb = getRepository(Cv).createQueryBuilder('cv');

  if (dto.title) {
    qb.andWhere('LOWER(cv.title) LIKE :title', { title: `${dto.title.toLowerCase()}%` });
  }
  if (dto.remote) {
    qb.andWhere('cv.remote = :remote', { remote: dto.remote });
  }
  if (dto.relocate) {
    qb.andWhere('cv.relocate = :relocate', { relocate: dto.relocate });
  }
  if (dto.enabled) {
    qb.andWhere('cv.enabled = :enabled', { enabled: dto.enabled });
  }
  if (dto.countryId) {
    qb.andWhere('cv.countryId = :countryId', { countryId: dto.countryId });
  }
  if (dto.cityId) {
    qb.andWhere('cv.cityId = :cityId', { cityId: dto.cityId });
  }
  if (dto.userId) {
    qb.andWhere('cv.userId = :userId', { userId: dto.userId });
  }

  qb.skip(dto.skip);
  qb.take(dto.take);

  qb.leftJoinAndSelect('cv.user', 'user');

  if (dto.categoriesIds) {
    qb.leftJoinAndSelect('cv.categories', 'categories', 'categories.id IN (:...categoriesIds)', {
      categoriesIds: dto.categoriesIds,
    });
  } else {
    qb.leftJoinAndSelect('cv.categories', 'categories');
  }

  qb.leftJoinAndSelect('cv.country', 'country');
  qb.leftJoinAndSelect('cv.city', 'city');
  qb.leftJoinAndSelect('cv.currency', 'currency');

  return qb.getManyAndCount();
}
