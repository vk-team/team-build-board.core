import { Cv } from '../common/entities/cv';
import { Pagination } from '../types';

export class CreateCvDto
  implements Partial<Omit<Cv, 'id' | 'categories' | 'city' | 'country' | 'currency' | 'user' | 'afterLoad'>> {
  title: string;

  salary: number;

  currencyId: number;

  description: string;

  userId: number;

  categoriesIds?: number[];

  relocate?: boolean;

  remote?: boolean;

  enabled?: boolean;

  countryId?: number;

  cityId?: number;
}

export class FindCv implements Partial<Pick<Cv, 'id' | 'userId'>> {
  id?: number;

  userId?: number;
}

export class FindCvs
  implements
    Partial<
      Omit<
        Cv,
        'id' | 'description' | 'country' | 'city' | 'currency' | 'user' | 'categories' | 'salary' | 'afterLoad'
      > &
        Pagination
    > {
  title?: string;

  relocate?: boolean;

  remote?: boolean;

  countryId?: number;

  enabled?: boolean;

  cityId?: number;

  currencyId?: number;

  userId?: number;

  minSalary?: number;

  maxSalary?: number;

  categoriesIds?: number[];

  take?: number;

  skip?: number;
}

export class UpdateCvDto implements Partial<CreateCvDto> {
  title?: string;

  salary?: number;

  relocate?: boolean;

  remote?: boolean;

  enabled?: boolean;

  description?: string;

  countryId?: number;

  cityId?: number;

  currencyId?: number;

  userId?: number;

  categoriesIds?: number[];
}
