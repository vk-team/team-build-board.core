import HttpStatus from 'http-status-codes';
import Router from 'koa-router';
import qs from 'qs';
import authorization from '../auth/authorization';
import { AppContext } from '../types';
import { createCv, deleteCvById, findCv, findCvs, updateCvById } from './db';

const router = new Router({ prefix: '/cvs' });
router
  .post('/', authorization(), async (ctx: AppContext) => {
    ctx.body = {
      data: await createCv({ ...ctx.request.body, userId: ctx.state.authUser.id }),
    };
  })
  .get('/', authorization(false), async (ctx: AppContext) => {
    const query = { ...qs.parse(ctx.request.query), enabled: true };
    query.userId = query.userId && parseInt(ctx.query.userId, 10);
    if (query.userId && ctx.state.authUser && query.userId === ctx.state.authUser.id) {
      query.enabled = ctx.query.enabled;
    }

    const [cvs, count] = await findCvs(query);
    ctx.body = {
      data: cvs,
      count,
    };
  })
  .put('/:id', authorization(), async (ctx: AppContext) => {
    const id = parseInt(ctx.params.id, 10);
    const cv = await findCv({ id, userId: ctx.state.authUser.id });
    if (!cv) {
      ctx.throw(HttpStatus.BAD_REQUEST, 'Cv not found');
    }

    ctx.body = {
      data: await updateCvById(id, { ...ctx.request.body, userId: ctx.state.authUser.id }),
    };
  })
  .delete('/:id', authorization(), async (ctx: AppContext) => {
    const id = parseInt(ctx.params.id, 10);
    const cv = await findCv({ id, userId: ctx.state.authUser.id });
    if (!cv) {
      ctx.throw(HttpStatus.BAD_REQUEST, 'Cv not found');
    }

    const { affected } = await deleteCvById(id);
    ctx.body = {
      data: { affected, id },
    };
  })
  .get('/:id', authorization(false), async (ctx: AppContext) => {
    const id = parseInt(ctx.params.id, 10);
    const cv = await findCv({ id });
    const userOwner = ctx.state.authUser && ctx.state.authUser.id === cv.userId;
    if (cv && (userOwner || cv.enabled === true)) {
      ctx.body = {
        data: cv,
      };
    } else {
      ctx.throw(HttpStatus.BAD_REQUEST, 'Cv not found');
    }
  });

export default router.routes();
