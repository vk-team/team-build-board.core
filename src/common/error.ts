import HttpStatus from 'http-status-codes';
import { Next } from 'koa';
import { ValidationError } from 'yup';
import { AppContext } from '../types';
import { formatYupErrors } from './format-yup-errors';

export default async function error(ctx: AppContext, next: Next) {
  try {
    await next();
  } catch (e) {
    let { message, status = HttpStatus.INTERNAL_SERVER_ERROR } = e;
    let validation;
    if (e instanceof ValidationError) {
      status = HttpStatus.BAD_REQUEST;
      message = 'Invalid data';
      validation = formatYupErrors(e);
    }

    ctx.status = status;
    ctx.body = {
      message,
      validation,
    };
    ctx.app.emit('error', e, ctx);
  }
}
