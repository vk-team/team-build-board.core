import Router from 'koa-router';
import { AppContext } from '../types';

const router = new Router();
router.get('/health-check', (ctx: AppContext) => {
  ctx.body = {
    data: {
      status: 'ok',
    },
  };
});

export default router.routes();
