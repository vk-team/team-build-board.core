import { createConnection, getConnection } from 'typeorm';
import { PG_URL } from '../config';
import { Category } from './entities/category';
import { City } from './entities/city';
import { Country } from './entities/country';
import { Currency } from './entities/currency';
import { Cv } from './entities/cv';
import { Team } from './entities/team';
import { User } from './entities/user';

export async function pgConnect() {
  return createConnection({
    type: 'postgres',
    url: PG_URL,
    synchronize: true,
    entities: [User, Category, Cv, Country, City, Currency, Team],
  });
}
export async function pgDisconnect() {
  return getConnection().close();
}
export async function pgSync() {
  return getConnection().synchronize(true);
}
