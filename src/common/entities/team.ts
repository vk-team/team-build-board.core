import {
  AfterLoad,
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Country } from './country';
import { City } from './city';
import { Category } from './category';
import { User } from './user';
import { serializeToPublic } from '../../users/serializer';

@Entity()
export class Team {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ default: true })
  enabled: boolean;

  @Column()
  description: string;

  @Column({ nullable: true })
  countryId: number;

  @ManyToOne(() => Country)
  @JoinColumn()
  country?: Country;

  @Column({ nullable: true })
  cityId: number;

  @ManyToOne(() => City)
  @JoinColumn()
  city?: City;

  @Column()
  ownerId: number;

  @ManyToOne(() => User)
  @JoinColumn()
  owner: ReturnType<typeof serializeToPublic>;

  @ManyToMany(() => Category, { cascade: true })
  @JoinTable()
  categories: Category[];

  @ManyToMany(() => User, { cascade: true })
  @JoinTable()
  users: ReturnType<typeof serializeToPublic>[];

  @AfterLoad()
  afterLoad() {
    this.users = this.users.map(serializeToPublic);
    this.owner = serializeToPublic(<User>this.owner);
  }
}
