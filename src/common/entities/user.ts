import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ROLES } from '../../users/constants';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  email: string;

  @Column()
  fullName: string;

  @Column()
  passwordHash: string;

  @Column({ type: 'enum', enum: Object.values(ROLES), default: ROLES.USER })
  role: ROLES;
}
