import {
  AfterLoad,
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Currency } from './currency';
import { Country } from './country';
import { City } from './city';
import { Category } from './category';
import { User } from './user';
import { serializeToPublic } from '../../users/serializer';

@Entity()
export class Cv {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({ type: 'decimal', scale: 2 })
  salary: number;

  @Column({ default: true })
  enabled: boolean;

  @Column({ default: false })
  relocate: boolean;

  @Column({ default: false })
  remote: boolean;

  @Column()
  description: string;

  @Column({ nullable: true })
  countryId: number;

  @ManyToOne(() => Country)
  @JoinColumn()
  country?: Country;

  @Column({ nullable: true })
  cityId: number;

  @ManyToOne(() => City)
  @JoinColumn()
  city?: City;

  @Column()
  currencyId: number;

  @ManyToOne(() => Currency)
  @JoinColumn()
  currency: Currency;

  @ManyToMany(() => Category, { cascade: true })
  @JoinTable()
  categories: Category[];

  @Column()
  userId: number;

  @ManyToOne(() => User)
  user: ReturnType<typeof serializeToPublic>;

  @AfterLoad()
  afterLoad() {
    this.salary = parseFloat(<string>(<unknown>this.salary));
    this.user = serializeToPublic(<User>this.user);
  }
}
