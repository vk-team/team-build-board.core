import { ValidationError } from 'yup';

export const formatYupErrors = (error: ValidationError) => {
  if (!error.inner.length && error.path) {
    return {
      [error.path]: error.errors,
    };
  }
  return error.inner.reduce((errors, e) => ({ ...errors, [e.path]: e.errors }), {});
};
