import faker from 'faker';
import _ from 'lodash';
import { createCategory } from '../../categories/db';
import { CreateCategoryDto } from '../../categories/dto';
import { createCurrency } from '../../currency/db';
import { createCv } from '../../cv/db';
import { CreateCvDto } from '../../cv/dto';
import { createCity, createCountry } from '../../locations/db';
import { createTeam } from '../../teams/db';
import { CreateTeamDto } from '../../teams/dto';
import { ROLES } from '../../users/constants';
import { createUser } from '../../users/db';
import { CreateUserDto } from '../../users/dto';
import { pgConnect, pgDisconnect, pgSync } from '../db';

async function createFakeData() {
  await pgConnect();
  await pgSync();

  const usersData = _.times(50, () => {
    const password = faker.internet.password();
    return <CreateUserDto>{
      email: faker.internet.email(),
      fullName: faker.internet.userName(),
      password,
      passwordRepeat: password,
      role: ROLES.USER,
    };
  });
  const users = await Promise.all(usersData.map(createUser));

  const currencies = await Promise.all([
    createCurrency({ name: 'USD' }),
    createCurrency({ name: 'EUR' }),
    createCurrency({ name: 'UAH' }),
  ]);

  const categoriesData = _.times(7, () => {
    return <CreateCategoryDto>{
      name: faker.lorem.words(2),
      enabled: faker.random.boolean(),
    };
  });
  const categories = await Promise.all(categoriesData.map(createCategory));

  const countriesData = _.times(10, () => {
    return {
      name: faker.lorem.words(2),
    };
  });
  const countries = await Promise.all(countriesData.map(createCountry));
  const citiesData = _.times(7, () => {
    return {
      name: faker.lorem.words(2),
      countryId: countries[faker.random.number(countries.length - 1)].id,
    };
  });
  const cities = await Promise.all(citiesData.map(createCity));
  const getLocation = () => {
    const country = countries[faker.random.number(countries.length - 1)];
    const citiesByCountry = cities.filter(c => c.countryId === country.id);
    const city = citiesByCountry[faker.random.number(citiesByCountry.length - 1)];

    return city ? { city, country } : getLocation();
  };

  const cvsData = _.times(100, () => {
    const { city, country } = getLocation();
    return <CreateCvDto>{
      title: faker.lorem.words(4),
      salary: parseFloat(faker.finance.amount()),
      description: faker.lorem.text(3),
      userId: users[faker.random.number(users.length - 1)].id,
      currencyId: currencies[faker.random.number(currencies.length - 1)].id,
      categoriesIds: categories.slice(faker.random.number(categories.length - 1)).map(c => c.id),
      countryId: country.id,
      cityId: city.id,
    };
  });
  await Promise.all(cvsData.map(createCv));

  const teamsData = _.times(100, () => {
    const { city, country } = getLocation();
    return <CreateTeamDto>{
      name: faker.lorem.words(4),
      enabled: faker.random.boolean(),
      description: faker.lorem.text(3),
      ownerId: users[faker.random.number(users.length - 1)].id,
      categoriesIds: categories.slice(faker.random.number(categories.length - 1)).map(c => c.id),
      countryId: country.id,
      cityId: city.id,
    };
  });
  await Promise.all(teamsData.map(createTeam));
  await pgDisconnect();
}
createFakeData().then(r => r);
