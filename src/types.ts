import { Context } from 'koa';
import { User } from './common/entities/user';

export type AppContext = Context & {
  state: {
    authUser?: User;
    jwtOriginalError?: {
      name: string;
      message: string;
      expiredAt: number;
    };
    tokenPayload?: {
      userId: number;
    };
  };
};

export type Pagination = {
  skip: number;

  take: number;
};
